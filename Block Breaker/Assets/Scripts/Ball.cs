﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

    private Paddle paddle;
    private bool hasStarted = false;    
    private Rigidbody2D rb;
    private Vector3 paddleToBallVector;

    private AudioSource boing;

	void Start ()
    {
        boing = gameObject.GetComponent<AudioSource>();
        rb = gameObject.GetComponent<Rigidbody2D>();
        paddle = GameObject.FindObjectOfType<Paddle>();
        paddleToBallVector = transform.position - paddle.transform.position;
	}
	
    void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 tweak = new Vector2(Random.Range(0f, 0.2f), Random.Range(0f, 0.2f));
        if (hasStarted)
        {
            rb.velocity += tweak;
            boing.Play();
        }
    }


	void Update ()
    {
        if (!hasStarted)
        {
            //Lock the ball relative to paddle
            transform.position = paddle.transform.position + paddleToBallVector;

            //Wait for a mouse press to launch.
            if (Input.GetMouseButtonDown(0))
            {
                print("Mouse clicked, launch ball");
                rb.velocity = new Vector2(2f, 10f);
                hasStarted = true;
            }
        }
	}

}
