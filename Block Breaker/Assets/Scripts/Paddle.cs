﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour {

    public bool autoPlay = false;

    private Ball ball;

    void Start ()
    {
        ball = GameObject.FindObjectOfType<Ball>();	
	}
	
	void Update ()
    {
        if (!autoPlay)
        {
            MoveWithMouse();
        }
        else
        {
            AutoPlay();
        }
	}

    void AutoPlay()
    {
        float min = 0.5f;
        float max = 15.5f;
        Vector3 paddlePos = new Vector3(8f, transform.position.y, -5f);
        Vector3 ballPos = ball.transform.position;
        paddlePos.x = Mathf.Clamp(ballPos.x, min, max);
        transform.position = paddlePos;
    }

    void MoveWithMouse()
    {
        float min = 1.29f;
        float max = 14.7f;
        float mousePosInBlocks = Input.mousePosition.x / Screen.width * 16;
        Vector3 paddlePos = new Vector3(8f, gameObject.transform.position.y, -5f);
        paddlePos.x = Mathf.Clamp(mousePosInBlocks, min, max);
        transform.position = paddlePos;
    }
}
