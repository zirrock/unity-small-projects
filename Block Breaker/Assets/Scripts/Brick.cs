﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brick : MonoBehaviour {

    public Sprite[] hitSprites;
    public static int breakableCount=0;
    public AudioClip crack;
    public GameObject smoke;
    [HideInInspector] public int timesHit;

    bool isBreakable;
    private LevelManager levelManager;


	void Start ()
    {
        isBreakable = (tag == "Breakable");
        if (isBreakable)
        {
            breakableCount++;
        }
        levelManager = GameObject.FindObjectOfType<LevelManager>();
        timesHit = 0;
    }
	
    void OnCollisionEnter2D(Collision2D collision)
    {
        timesHit++;
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (isBreakable)
        {
            HandleHits();
        }
    }

    void HandleHits()
    {
        int maxHits = hitSprites.Length + 1;
        if (timesHit >= maxHits)
        {
            breakableCount--;
            AudioSource.PlayClipAtPoint(crack, transform.position);
            levelManager.BrickDestroyed();
            GameObject smokePuff = Instantiate(smoke, transform.position, Quaternion.identity) as GameObject;
            ParticleSystem.MainModule settings = smokePuff.GetComponent<ParticleSystem>().main;
            settings.startColor = new ParticleSystem.MinMaxGradient(gameObject.GetComponent<SpriteRenderer>().color);
            Destroy(gameObject);
        }
        else
        {
            LoadSprites();
        }
    }

    void LoadSprites()
    {
        int spriteIndex = timesHit - 1;
        if (hitSprites[spriteIndex])
        {
            GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
        }
        else
        {
            Debug.LogError("Brick sprite missing!");
        }
    }
}
