﻿using UnityEngine;
using System.Collections;
using UnityEditor.SceneManagement;

public class LevelManager : MonoBehaviour {

	public void LoadLevel(string name)
    {
		Debug.Log ("New Level load: " + name);
		EditorSceneManager.LoadScene(name);
	}

	public void QuitRequest()
    {
		Debug.Log ("Quit requested");
		Application.Quit ();
	}

}
