﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    public float health = 150;
    public float projectileSpeed;
    public GameObject projectile;
    public float shotsPerSecond;
    public int scoreValue = 150;
    private ScoreKeeper scoreKeeper;

    public AudioClip fireSound;
    public AudioClip deathSound;

    private void Start()
    {
        scoreKeeper = GameObject.Find("Score").GetComponent<ScoreKeeper>();

    }

    void Update()
    {
        float probabilty = Time.deltaTime * shotsPerSecond;
        if (Random.value < probabilty)
        {
            Fire();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        Projectile missile = other.gameObject.GetComponent<Projectile>();
        if(missile)
        {
            health -= missile.GetDamage();
            missile.Hit();
            if(health <= 0)
            {
                Die();  
            }
            Debug.Log("Hit by a projectile");
        }
    }

    void Fire()
    {
        AudioSource.PlayClipAtPoint(fireSound, transform.position);
        Vector3 startPosition = transform.position + new Vector3(0, -1, 0);
        GameObject beam = Instantiate(projectile, startPosition, Quaternion.identity) as GameObject;
        beam.GetComponent<Rigidbody2D>().velocity = new Vector3(0, -projectileSpeed, 0);
    }

    void Die()
    {
        scoreKeeper.Score(scoreValue);
        AudioSource.PlayClipAtPoint(deathSound, transform.position);
        Destroy(gameObject);
    }
}
