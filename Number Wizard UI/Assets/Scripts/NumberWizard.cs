﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NumberWizard : MonoBehaviour {

    int max, min, guess;

    public int maxGuessesAllowed = 10;

    public Text text;

    void StartGame()
    {
        max = 1000;
        min = 1;
        NextGuess();
    }

    void NextGuess()
    {
        guess = Random.Range(min, max+1);
        text.text = guess.ToString();
        maxGuessesAllowed -= 1;
        if(maxGuessesAllowed <=0)
        {
            SceneManager.LoadScene("Win", LoadSceneMode.Single);
        }

    }

    void Start ()
    {
        StartGame();
	}

    public void GuessHiger()
    {
        min = guess;
        NextGuess();
    }

    public void GuessLower()
    {
        max = guess;
        NextGuess();
    }
}
