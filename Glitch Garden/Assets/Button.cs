﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour {

    private Button[] buttonArray;

	void Start ()
    {
        buttonArray = GameObject.FindObjectOfType<Button>();
	}
	
	void Update () {
		
	}

    void OnMouseDown()
    {
        foreach(Button thisButton in Button[])
        {
            thisButton.GetComponent<SpriteRenderer>().color = Color.black;
        }

        GetComponent<SpriteRenderer>().color = Color.white;
    }
}
