﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

    public float speed, damage;

	// Use this for initialization
	void Start () {
		
	}
	
	void Update ()
    {
        transform.Translate(Vector3.right * speed * Time.deltaTime);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.GetComponent<Attacker>())
        {
            Health health = other.gameObject.GetComponent<Health>();
            if (health)
            {
                health.DealDamage(damage);
            }
            SelfDestruct();
        }
    }

    public void SelfDestruct()
    {
        Destroy(gameObject);
    }

}
