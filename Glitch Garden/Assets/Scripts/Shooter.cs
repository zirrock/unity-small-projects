﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {

    public GameObject projectile;
    private GameObject projectileParent;
    public GameObject gun;

	void Start ()
    {
        projectileParent = GameObject.Find("Projectiles");
	}
	
	private void Fire()
    {
        GameObject newProjectile = Instantiate(projectile,gun.transform.position,Quaternion.identity);
        newProjectile.transform.parent = projectileParent.transform;
    }
}
