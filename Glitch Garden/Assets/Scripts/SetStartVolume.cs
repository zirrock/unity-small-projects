﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetStartVolume : MonoBehaviour {


    private MusicManager musicManager;

	void Start ()
    {
        musicManager = FindObjectOfType<MusicManager>();
        if (!musicManager)
        {
            Debug.LogWarning("No music manager found");
        }
        else
        {
            float volume = PlayerPrefsManager.GetMasterVolume();
            musicManager.ChangeVolume(volume);
        }
	}
	
}
