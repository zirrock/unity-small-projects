﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour {

    public float health;

	// Use this for initialization
	void Start () {
		
	}
	
	void Update ()
    {
	}

    public void DealDamage(float damage)
    {
        health -= damage;
        if (health <= 0)
        {
            KillObject(gameObject);
        }
    }

    private void KillObject(GameObject target)
    {
        Destroy(target);
    }

}
