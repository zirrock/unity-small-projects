﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour {

    private float currentSpeed;
    private GameObject currentTarget;
    private Animator anim;

	void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
	void Update ()
    {
        transform.Translate(Vector3.left * currentSpeed * Time.deltaTime);	
        if(!currentTarget)
        {
           anim.SetBool("Is Attacking", false);
        }
	}

    void OnTriggerEnter2D(Collider2D other)
    {
       // Debug.Log(other + "trigger enter");
    }

    public void SetSpeed(float speed)
    {
        currentSpeed = speed;
    }

    //called from the animator when striking
    public void StrikeCurrentTarget(float damage)
    {
        Debug.Log(name + " dealt " + damage + " damage");
        if (currentTarget)
        {
            Health health = currentTarget.GetComponent<Health>();
            if (health)
            {
                health.DealDamage(damage);
            }
        }
    }

    public void Attack(GameObject target)
    {
        currentTarget = target;
    }
}
