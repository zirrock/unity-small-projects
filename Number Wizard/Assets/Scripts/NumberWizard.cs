﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NumberWizard : MonoBehaviour {

    int max, min, guess;

    void StartGame()
    {
        max = 1000;
        min = 1;
        guess = 500;

        print("========================");
        print("Welcome to Number Wizard");
        print("Pick a number in your head, but don't tell me");

        print("The highest number you can pick is " + max);
        print("The lowest number you can pick is " + min);

        print("Is the number higher or lower than 500?");
        print("Up = higher, down = lower, return = equals");

        max += 1;
    }

    void NextGuess()
    {
        guess = (min + max) / 2;
        print("Up = higher, down = lower, return = equals");
    }

    void Start ()
    {
        StartGame();
	}
	
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            print("Up arrow key");
            min = guess;
            NextGuess();
        } else if(Input.GetKeyDown(KeyCode.DownArrow))
        {
            print("Down arrow key");
            max = guess;
            NextGuess();
        } else if (Input.GetKeyDown(KeyCode.Return))
        {
            print("I won!");
            StartGame();
        }
    }
}
